<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->increments('id');
            $table->string( 'gender', 6 );
            $table->string( 'firstname', 255 );
            $table->string( 'infixname', 255 )->nullable();
            $table->string( 'surname', 255 );
            $table->date( 'date_of_birth' );
            $table->string( 'zipcode', 7 );
            $table->string( 'house_num', 10 );
            $table->string( 'house_num_add', 10 )->nullable();
            $table->string( 'street', 255 );
            $table->string( 'city', 255 );
            $table->string( 'telephone', 30 )->nullable();
            $table->string( 'emailaddress', 255 );
            $table->boolean( 'kit' )->default( false );

            $table->string( 'origin', 255 )->nullable();
            $table->string( 'user_type', 10 );
            $table->ipAddress( 'ipaddress' );
            $table->string( 'fingerprint', 40 );
            $table->text( 'user_agent' )->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
