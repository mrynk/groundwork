<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any( '/', [HomeController::class, 'index'] );
Route::any( '/share/{id}', [HomeController::class, 'share'] );

Route::any( '/mail/confirm/{publicId}', [HomeController::class, 'mailPreview'] );

/* Route::get( '/{wildcard}/{wildcard2?}', function(){
    return redirect('/');
}); */
