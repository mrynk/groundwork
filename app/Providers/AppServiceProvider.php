<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $hash = trim( file_get_contents( resource_path('hash.txt') ) );
        config(['app.decache' => $hash ]);
        \Blade::directive('decache', function ($expression) {
            return "<?php echo str_replace( '.hash.', '.' . config('app.decache') . '.', '$expression' ); ?>";
        });
    }
}
