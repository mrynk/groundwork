<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MailMX implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes( $attribute, $value )
    {
        list( $user, $domain ) = explode( '@', $value );
        return \Cache::rememberForever( 'mailmx:' . $domain, function () use ( $domain )
        {
            return checkdnsrr( $domain, 'MX' );
        });
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The maildomain is invalid.';
    }
}
