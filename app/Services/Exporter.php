<?php

namespace App\Services;

class Exporter
{
    public function __construct( $builder, $separator = ',', $chunksize = 1000 )
    {
        $this->_chunksize = $chunksize;
        $this->_selection = $builder;
        $this->_repeat = null;
        $this->_separator = $separator;
    }

    /**
     * Define the columns and transformers
     *
     * @param array $transformers
     * @return \Exporter
     */
    public function columns( $transformers )
    {
        $this->_columns = $transformers;
        return $this;
    }

    /**
     * repeat by column
     *
     * @param array $transformers
     * @return \Exporter
     */
    public function repeat( $column )
    {
        $this->_repeat = $column;
        return $this;
    }

    /**
     * Define the columns and transformers
     *
     * @param string $filename
     * @return string
     */
    public function export( $filename, $path = '' )
    {
        $this->_total = 0;
        if( strpos( $filename, ':total:' ) !== false )
        {
            $this->_total = $this->_selection->count();
        }
        $this->_fspath = storage_path( $path );
        $this->_fsname = str_replace( [ ':total:' ], [ $this->_total ], $filename );
        $fp = fopen( $this->_fspath . '/' . $this->_fsname, 'w' );
        fwrite( $fp, implode( $this->_separator, array_keys( $this->_columns ) ) . PHP_EOL );

        $_repeat = $this->_repeat;
        $_columns = $this->_columns;
        $this->_selection->chunk( $this->_chunksize, function( $rows, $page ) use ( $fp, $_repeat, $_columns )
        {
            foreach( $rows as $row )
            {
                $data = [];
                $repeat = 1;
                if( $_repeat )
                    $repeat = is_string( $_repeat ) ? $row->{$_repeat} : $_repeat( $row );
                foreach( $_columns as $column )
                {
                    $data[] = is_string( $column ) ? $row->{$column} : $column( $row );
                }
                for( $i = 0; $i < $repeat; $i++ )
                {
                    fwrite( $fp, implode( $this->_separator, $data ) . PHP_EOL );
                }
            }
            fflush( $fp );
        });

        fclose($fp);
        return $this;
    }

    public function getFilePath()
    {
        return $this->_fspath;
    }

    public function getFileName()
    {
        return $this->_fsname;
    }

    public function getFullFilePath()
    {
        return $this->_fspath . '/' . $this->_fsname;
    }
}
