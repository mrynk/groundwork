<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mail\ConfirmMail;
use Illuminate\Support\Facades\Mail;

use GuzzleHttp\Client;

use App\Rules\MailMX;
use App\Models\Entry;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Entry::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'user_agent' => $request->header('User-Agent'),
            'ipaddress' => $request->ip(),
            'user_type' => $this->botCheck( $request )
        ]);

        Entry::$rules['emailaddress'][] = new MailMX();

        $this->validate( $request, Entry::$rules );

        try {
            $entry = Entry::create( $request->all() );
        }
        catch( \Exception $e )
        {
            \Log::warning( 'Deelname kon niet verwerkt worden', [ $e, $request->all() ] );
            return abort( 500, 'Deelname kon niet verwerkt worden' );
        }

        if( $entry->user_type !== 'bot' )
        {
            try {
                Mail::to( $entry->emailaddress )->send( new ConfirmMail( $entry ) );
            }
            catch( \Exception $e )
            {
                \Log::warning( 'Er is een email mislukt', [ $entry, $e ] );
            }
        }

        return $entry;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function botCheck( $request )
    {
        $result = 'user';
        try {
            $client = new Client([
               'base_uri' => 'https://www.google.com/'
            ]);
            $response = $client->post( '/recaptcha/api/siteverify', [
               'query' => [
                   'secret' => env( 'RECAPTCHA_SECRET', '' ),
                   'response' => $request->get('g-recaptcha-response'),
                   'remoteip' => $request->ip()
               ]
            ]);
            $responseJson = json_decode( $response->getBody(), true );

            if( !$responseJson['success'] )
                $result = 'bot';
        } catch(Exception $e) {
            $result = 'not-sure';
        }
        return $result;
    }
}
