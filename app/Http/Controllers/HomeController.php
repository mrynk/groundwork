<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\ConfirmMail;
use App\Models\Entry;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'base', [
            'share' => [
                'title' => 'General Title',
                'description' => 'General Description',
                'image' => \URL::secure('/static/img/share.jpg'),
                'url' => \URL::secure('/')
            ]
        ] );
    }

    public function share( $id )
    {
        $entry = Entry::findOrFail( $id );

        return view( 'base', [
            'entry' => $entry,
            'share' => [
                'title' => 'Entry Title',
                'description' => 'Entry Description',
                'image' => \URL::secure('/storage/'.$entry->id.'.jpg'),
                'url' => \URL::secure('/share/'.$entry->id.'.jpg')
            ]
        ] );
    }

    public function mailPreview( Request $request, $publicId )
    {
        $parts = explode( '-', $publicId );
        if( count( $parts ) < 2 )
        {
            return redirect('/');
        }
        list( $hash, $id ) = $parts;
        $entry = Entry::find( $id );
        if( !$entry || $hash !== $entry->uuid )
        {
            return redirect('/');
        }
        $mail = new ConfirmMail( $entry );
        return $mail;
    }
}
