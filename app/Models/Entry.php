<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prize',
        'score',
        'firstname',
        'infixname',
        'surname',
        'date_of_birth',
        'zipcode',
        'house_num',
        'house_num_add',
        'street',
        'city',
        'telephone',
        'emailaddress',
        'kit',
        'origin',
        'user_type',
        'ipaddress',
        'fingerprint',
        'user_agent',
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = [
        'id'
    ];

    public static $rules = [
        'firstname' => 'required|min:2|max:40|regex:/^[A-Za-z0-9 ]*$/i',
        'infixname' => 'sometimes|nullable|min:2|regex:/^[A-Za-z0-9 \']*$/i',
        'surname' => 'required|min:2|max:80|regex:/^[A-Za-z0-9 ]*$/i',
        'zipcode' => [ 'required', 'regex:/^[1-9][0-9]{3}[A-Z]{2}$/' ],
        'house_num' => [ 'required', 'regex:/^[1-9]{1}[0-9]{0,4}$/' ],
        'house_num_add' => [ 'regex:/^[A-Za-z0-9 ]*$/i', 'max:30' ],
        'street' => 'required|min:2|max:80|regex:/^[A-Za-z0-9 ]*$/i',
        'date_of_birth' => 'required|date_format:"Y-m-d"|before:-18 years',
        'city' => 'required|min:2|max:80|regex:/^[A-Za-z0-9 ]*$/i',
        'telephone' => [ 'sometimes', 'nullable', 'regex:/^0[0-9]{9}$/i' ],
        'emailaddress' => ['required', 'email', 'max:80'],
        'ipaddress' => 'required|ip',
        'user_type' => 'in:user,bot',
        'fingerprint' => 'required|size:32',
    ];
}
