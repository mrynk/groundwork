<?php

namespace App;

use MadWeb\Initializer\Contracts\Runner;
use \MadWeb\Initializer\Jobs\MakeCronTask;

class Install
{
    public function production(Runner $run)
    {
        $run->external('composer', 'install', '--no-dev', '--prefer-dist', '--optimize-autoloader')
            ->artisan('key:generate', [ '--force' => true ])
            ->artisan('migrate', [ '--force' => true, '--seed' => true ])
            ->artisan('storage:link')
            ->dispatch(new MakeCronTask)
            // ->external('npm', 'install', '--production')
            // ->external('npm', 'run', 'production')
            ->artisan('route:cache')
            ->artisan('config:cache')
            ->artisan('event:cache');
    }

    public function accept(Runner $run)
    {
        $run->external('composer', 'install')
            ->artisan('key:generate')
            ->artisan('migrate')
            ->artisan('storage:link')
            ->dispatch(new MakeCronTask);
            // ->external('npm', 'install')
            // ->external('npm', 'run', 'development');
    }

    public function local(Runner $run)
    {
        $run->external('composer', 'install')
            ->artisan('key:generate')
            ->artisan('migrate')
            ->artisan('storage:link')
            ->dispatch(new MakeCronTask);
            // ->external('npm', 'install')
            // ->external('npm', 'run', 'development');
    }
}
