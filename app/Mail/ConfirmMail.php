<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( \App\Entry $entry )
    {
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'score' => $this->entry->score,
            'name' => $this->entry->firstname,
            'has_shared' => $this->entry->has_shared,
            'weburl' => url( 'mail/confirm/' . $this->entry->uuid . '-' . $this->entry->id ),
            'shareurl' => url( 'share/' . $this->entry->id ),
        ];

        return $this->subject('Bedankt voor je deelname!')->view('mail.confirm3', $data );
    }
}
