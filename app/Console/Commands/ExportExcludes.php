<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\Exporter;

class ExportExcludes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:excludes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export exclude leads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = \DB::table('entries')
        ->select( \DB::raw('emailaddress, MAX(firstname) as firstname') )
        ->where( 'kit', 1 )
        ->groupBy( 'emailaddress' )
        ->orderBy( 'emailaddress' );
        $filename = env('APP_NAME') . '-excludelist.csv';

        $export = (new Exporter( $query ))->columns( [
            'Voornaam' => 'firstname',
            'E-mail' => 'emailaddress'
        ] )->export( $filename );
    }
}
