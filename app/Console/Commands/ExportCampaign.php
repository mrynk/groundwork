<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

use App\Services\Exporter;

class ExportCampaign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:campaign {date=now}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export daily campaign data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = \Carbon\Carbon::createFromDate( 2019, 7, 11, 0, 0, 0 );

        $liveUrl = \URL::to('/');
        $leadSupplier = 'Mr. Ynk';
        $leadIdSupplier = 1080;
        $campaignId = 0;
        $campaignName = 'ntb';
        $leadSource = 'ntb';
        $leadType = 'Campagnelead'; // HotLead, Campagnelead, Affiliate
        $preferredBrand = 'STL'; // STL of LT
        $singleOptIn = true; // Of de Lead in een keer uitgevraagd wordt voor STL en of Lotto en alle kanalen
        $premium = '';
        $optinInText = 'Ja, ik wil éénmalig een telefonisch aanbod ontvangen van Staatloterij of Lotto om automatisch mee te spelen. Ja, Ik wil per e-mail berichten en aanbiedingen van de (toekomstige) kansspelen van de Nederlandse Loterij ontvangen.';

        $date = $this->argument('date');
        if( $date === 'now' )
        {
            $now = \Carbon\Carbon::now();
        } else {
            $dateParts = explode( '-', $date );
            $now = \Carbon\Carbon::createFromDate( $dateParts[0], $dateParts[1], $dateParts[2] );
        }
        $to = $now->startOfDay();
        $from = $to->copy()->subDays(1);
        $batchId = $from->diffInDays( $startDate );

        if( $now->lt( $startDate ) ) return;

        $entry = new \App\Entry();
        $columns = Schema::getColumnListing( $entry->getTable() );
        $selects = [];
        foreach( $columns as $column )
        {
            $selects[] = 'MAX(' . $column . ') AS ' . $column;
        }
        $query = \App\Entry::select( \DB::raw( implode( ', ', $selects ) ) )
            ->whereNotNull('date_of_birth')
            ->where( 'created_at', '>=', $from )
            ->where( 'created_at', '<', $to );
        $query = $query->where( function( $query )
        {
           $query->where( 'kit', '=', 1 )
                 ->orWhereNotNull('telephone');
        });
        $query = $query->groupBy( [ 'emailaddress' ])->orderBy( 'created_at', 'desc' );


        $filename = 'LEADS_' . $from->format('Ymd') . '_' . $leadSupplier . '_' . $leadSource . '_' . $batchId . '.csv';

        // gender

        $empty = function ( $row ) { return null; };

        $export = (new Exporter( $query, ';' ))->columns( [

            'INITIALS' => $empty,
            'SALUTATION' => function ( $row ) { return $row->gender === 'male' ? 'Mr.' : 'Mrs.'; },
            'FIRSTNAME' => 'firstname',
            'MIDDLENAME' => function ( $row ) { return $row->infixname; },
            'LASTNAME' => 'surname',
            'EMAIL' => 'emailaddress',
            'PHONE' => 'telephone',
            'MOBILEPHONE' => $empty,
            'BIRTHDATE' => function( $row ){ return \Carbon\Carbon::createFromFormat( 'Y-m-d', $row->date_of_birth )->format('d-m-Y'); },
            'IBAN' => $empty,
            'POSTALCODE' => 'zipcode',
            'HOUSE_NUMBER' => 'house_num',
            'HOUSE_NUMBER_EXT' => 'house_num_add',
            'STREET' => 'street',
            'CITY' => 'city',
            'SALESFORCE_ID' => $empty,
            'PARENT_LEAD_SALESFORCE_ID' => $empty,
            'NYX_ID' => $empty,
            'SINGLE_OPT_IN' => function() use ( $singleOptIn ) { return $singleOptIn ? 1 : null; },
            'PREFERRED_BRAND' => function() use ( $preferredBrand ) { return $preferredBrand; },
            'MULTIPLE_OPT_IN' => function() use ( $singleOptIn ) { return !$singleOptIn ? 1 : null; },
            'CMT' => $empty,
            'CMT_PARTICIPATION_CODE' => $empty,

            'LEAD_EJ_ACTIES_MARKET_FLAGS_EM' => $empty,
            'LEAD_EJ_ACTIES_MARKET_FLAGS_TM' => $empty,
            'LEAD_EJ_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_EJ_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_KL_ACTIES_MARKET_FLAGS_EM' => $empty,
            'LEAD_KL_ACTIES_MARKET_FLAGS_TM' => $empty,
            'LEAD_KL_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_KL_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_LD_ACTIES_MARKET_FLAGS_EM' => $empty,
            'LEAD_LD_ACTIES_MARKET_FLAGS_TM' => $empty,
            'LEAD_LD_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_LD_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_LOTTO_ACTIES_MARKET_FLAGS_EM' => function( $row ) use ( $preferredBrand ) { return $row->kit && $preferredBrand === 'LT' ? 1 : null; },
            'LEAD_LOTTO_ACTIES_MARKET_FLAGS_TM' => function( $row ) use ( $preferredBrand ) { return $row->telephone && $preferredBrand === 'LT' ? 1 : null; },
            'LEAD_LOTTO_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_LOTTO_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_MJS_ACTIES_MARKET_FLAGS_EM' => $empty,
            'LEAD_MJS_ACTIES_MARKET_FLAGS_TM' => $empty,
            'LEAD_MJS_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_MJS_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_NK_ACTIES_MARKET_FLAGS_EM' => $empty,
            'LEAD_NK_ACTIES_MARKET_FLAGS_TM' => $empty,
            'LEAD_NK_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_NK_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_NLO_ACTIES_MARKET_FLAGS_EM' => $empty,
            'LEAD_NLO_ACTIES_MARKET_FLAGS_TM' => $empty,
            'LEAD_NLO_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_NLO_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_STL_ACTIES_MARKET_FLAGS_EM' => function( $row ) use ( $preferredBrand ) { return $row->kit && $preferredBrand === 'STL' ? 1 : null; },
            'LEAD_STL_ACTIES_MARKET_FLAGS_TM' => function( $row ) use ( $preferredBrand ) { return $row->telephone && $preferredBrand === 'STL' ? 1 : null; },
            'LEAD_STL_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_STL_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_TOTO_ACTIES_MARKET_FLAGS_EM' => $empty,
            'LEAD_TOTO_ACTIES_MARKET_FLAGS_TM' => $empty,
            'LEAD_TOTO_ACTIES_MARKET_FLAGS_WHITEMAIL' => $empty,
            'LEAD_TOTO_ACTIES_MARKET_FLAGS_SMS' => $empty,
            'LEAD_SUPPLIER' => function() use ( $leadSupplier ) { return $leadSupplier; },
            'LEADSOURCE' => function() use ( $leadSource ) { return $leadSource; },
            'LEAD_ID_SUPPLIER' => function() use ( $leadIdSupplier ) { return $leadIdSupplier; },
            'BATCH_ID_LEADSUPPLIER' => function() use ( $batchId ) { return $batchId; },
            'BATCH_ID_CALLCENTER' => $empty,
            'URL_OPT_IN' => function() use ( $liveUrl ) { return $liveUrl; },
            'PREMIUM' => function() use ( $premium ) { return $premium; },
            'MARKETING_CODE' => $empty,
            'MARKETING_NAME' => $empty,
            'CAMPAIGNID' => function() use ( $campaignId ) { return $campaignId; },
            'CAMPAIGNNAME_TM' => function() use ( $campaignName ) { return $campaignName; },
            'CAMPAIGNNAME_EM' => function() use ( $campaignName ) { return $campaignName; },
            'MARKETINGPACK' => $empty,
            'LEADTYPE' => function() use ( $leadType ) { return $leadType; },
            'REGISTRATION_DATE_TIME_OPT_IN_1' => function( $row ) { return $row->created_at->format( 'd-m-Y H:i:s' ); },
            'DELIVERDATE' => function( ) use ( $now ) { return $now->format( 'd-m-Y' ); },
            'SELECTIONDATE' => $empty,
            'OPT_IN_TEXT_1' => function() use ( $optinInText ) { return $optinInText; },
            'TARGETGROUP' => $empty,
            'FREE_FORMAT_FIELD_1' => $empty,
            'FREE_FORMAT_FIELD_2' => $empty,
            'FREE_FORMAT_FIELD_3' => $empty,
            'FREE_FORMAT_FIELD_4' => $empty,
            'FREE_FORMAT_FIELD_5' => $empty,
            'BMNR_1' => $empty,
            'BMNR_2' => $empty,
            'ACQUISITION_CHANNEL' => $empty,
            'RESPONS_CHANNEL' => $empty,
            'REGISTRATION_SOURCE_CODE' => $empty,
            'REGISTRATION_SOURCE_NAME' => $empty,
            'CALL_DATE' => $empty,
            'CALL_TIME' => $empty,
            'EC' => $empty,
            'ECO' => $empty,
            'ECSTATUS' => $empty,
            'CALL_ATTEMPTS' => $empty,
            'AGENTID' => $empty,
            'ERRORMESSAGES' => $empty,
            'ERRORCODE' => $empty,
            'TEL_IN_BMNR' => $empty,
            'MOB_IN_BMNR' => $empty,
            'GAMECODE' => $empty,
            'SUBSCRIPTION_ID' => $empty,
            'REGISTRATION_DATE' => $empty,
            'NUMBER_OF_BOARDS' => $empty,
            'SUBSCRIPTION_DURATION_YEARS' => $empty,
            'SUSPENDDATE' => $empty,
            'CANCELLATION_DATE' => $empty,
            'ADD_ON' => $empty,
            'PROFILE_ID' => $empty,
            'DUPLICATE' => $empty

        ] )->export( $filename );
    }
}
