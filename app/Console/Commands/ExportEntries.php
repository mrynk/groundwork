<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

use App\Services\Exporter;

class ExportEntries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:entries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create export file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $entry = new \App\Entry();
        $columns = Schema::getColumnListing( $entry->getTable() );
        $selects = [];
        foreach( $columns as $column )
        {
            $selects[] = 'MAX(' . $column . ') AS ' . $column;
        }
        $query = \App\Entry::select( \DB::raw( implode( ', ', $selects ) ) );
        $filename = env('APP_NAME') . '-entries_' . date('Y-m-d') . '.csv';

        $export = (new Exporter( $query, ',' ))->columns( [
            'Firstname' => 'firstname',
            'Tussenvoegsel' => 'infixname',
            'Achternaam' => 'surname',
            'E-mail' => 'emailaddress',
            'E-mail Opt-in' => 'kit'
        ] )->export( $filename );
    }
}
