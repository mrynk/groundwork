<!DOCTYPE html>
<html lang="{{ \App::getLocale() }}" dir="ltr">
<head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">

    <title>{{ $share['title'] }}</title>
    <meta name="title" content="{{ $share['title'] }}">
    <meta name="description" content="{{ $share['description'] }}">

    <link rel="icon" type="image/png" href="/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="https://via.placeholder.com/180">
    <link rel="icon" type="image/png" href="https://via.placeholder.com/32" sizes="32x32">
    <link rel="icon" type="image/png" href="https://via.placeholder.com/16" sizes="16x16">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <meta property="og:url"             content="{{ $share['url'] }}" />
    <meta property="og:image"           content="{{ $share['image'] }}" />
    <meta property="og:type"            content="website" />
    <meta property="og:description"     content="{{ $share['description'] }}" />
    <meta property="og:site_name"       content="{{ $share['title'] }}" />
    <meta property="og:title"           content="{{ $share['title'] }}" />
    <meta property="fb:app_id"          content="{{ env( 'FB_ID', 'tba' ) }}" />

    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{ $share['url'] }}">
    <meta property="twitter:title" content="{{ $share['title'] }}">
    <meta property="twitter:description" content="{{ $share['description'] }}">
    <meta property="twitter:image" content="{{ $share['image'] }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="{{ $share['url'] }}" />

@if( env('APP_ENV') !== 'production' )
    <meta name="robots" content="noindex">
@endif

    <meta property="author"             content="Mr. Ynk - mrynk.nl" />

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','{{ env( "GTM_ID", "tba" ) }}');</script>
    <!-- End Google Tag Manager -->

    <script>
        dataLayer.push({ game: { brand: 'Brand', name: 'Name', page: { type: 'Promotion' } } });
    </script>

    <link href="@decache(/static/css/app.hash.css)" rel=stylesheet>
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ env( "GTM_ID", "tba" ) }}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id=app></div>

    <script>
        window._rootData = {
            apiUrl: '{{ URL::to("/") }}',
            r: '{{ \Request::getRequestUri() }}',
            recaptchaSitekey: '{{ env( "RECAPTCHA_SITEKEY", "tba" ) }}',
        }
    </script>

    <script src='{{ "https://www.google.com/recaptcha/api.js?render=" . env( "RECAPTCHA_SITEKEY" ) }}'></script>
    <script>
        grecaptcha.ready( function() {
            window.recaptchaReady = true;
        });
    </script>

    <script type=text/javascript src="@decache(/static/js/chunk-vendors.hash.js)"></script>
    <script type=text/javascript src="@decache(/static/js/app.hash.js)"></script>
</body>
</html>

